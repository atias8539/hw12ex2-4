#include "MessagesSender.h"
#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <thread>
#include <mutex>
#include <chrono>
std::mutex mtx;
void readFile(queue<string>& myQ);
void writeToFile(queue<string>& myQ,vector<string>& g1);
int main()
{
	queue<string> myqueue;
	MessagesSender m1;
	m1.signin();
	m1.signin();

	std::thread first(readFile,ref(myqueue));     // spawn new thread that calls foo()
	std::thread second(writeToFile,ref(myqueue),ref(m1.g1));  // spawn new thread that calls bar(0)
	first.join();                // pauses until first finishes
	second.join();
	system("pause");
}
void readFile(queue<string>& myQ)
{
	string line;

	ifstream file;
	ofstream outfile;
	mtx.lock();
	file.open("data.txt");
	outfile.open("newData.txt");
	while (getline(file, line))
	{
		myQ.push(line);	
	}
	outfile.close();
	file.close();
	remove("data.txt");
	rename("newData.txt", "data.txt");
	mtx.unlock();
}
void writeToFile(queue<string>& myQ ,vector<string>& g1)
{
	string line;
	string name;
	ofstream file;
	mtx.lock();
	file.open("outData.txt");
	
	while (!myQ.empty())
	{
		line = myQ.front();
		for (int i = 0; i < g1.size(); i++)
		{

			file << g1[i] << " - " << line<<std::endl;
		
		}
		myQ.pop();
	}
	mtx.unlock();
}